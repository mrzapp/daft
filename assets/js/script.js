'use strict';

// Global vars
let clipboard = '';
let clipboardMode = 'copy';

// Helpers
function loop(obj, cb) {
    if(!obj || obj instanceof Object !== true) { return ''; }

    let html = '';

    for(const k in obj) {
        html += cb(k, obj[k]);
    }

    return html;
}

function dirname(path) {
    return path.substring(0, path.lastIndexOf('/'));
}

function urlencode(uri) {
    return encodeURIComponent(uri).replace(/%2F/g, '/');
}

async function fetchWithRetry(url, options) {
    try {
        return await fetch(url, options);
    
    } catch(e) {
        if(confirm('Request failed. Retry?')) {
            return await fetchWithRetry(url, options);
        }
    }

    return null;
}

function toBase64(file) {
    const reader = new FileReader();

    reader.readAsDataURL(file);

    return new Promise((resolve, reject) => {
        reader.onload = () => {
            resolve(reader.result.split(',').pop());
        };
        
        reader.onerror = (e) => {
            reject(e);
        };
    });
}

async function update(e) {
    document.body.classList.toggle('-loading', true);

    const ctxResponse = await fetchWithRetry(`/api/context?path=${location.pathname}`);

    if(!ctxResponse) {
        document.body.classList.toggle('-loading', false);
        
        return;
    }

    const ctx = await ctxResponse.json();

    // Set head info
    document.querySelector('head title').innerText = ctx.title;
    document.querySelector('head link[rel="icon"]').href = `/assets/icons/${ctx.icon}.svg`;
    
    document.body.classList.toggle('-loading', false);

    // Render body
    document.body.innerHTML = `
        <header>
            <a id="back" ${ctx.current_rel_path !== '/' ? `href="${Object.keys(ctx.nav).at(-2)}"` : ''}></a>

            <nav>
                ${loop(ctx.nav, (uri, segment) => `
                    <a ${uri !== ctx.current_rel_path ? `href="${urlencode(uri)}"` : ''}>${segment}</a>
                `)}
            </nav>
            
            ${ctx.is_dir ? `
                <details id="playlist" title="Create playlist from folder">
                    <summary></summary>
                    <menu>
                        <menuitem>
                            <a href="/api/m3u8?path=${ctx.current_rel_uri}&type=audio">Audio</a>
                        </menuitem>
                        <menuitem>
                            <a href="/api/m3u8?path=${ctx.current_rel_uri}&type=image">Images</a>
                        </menuitem>
                        <menuitem>
                            <a href="/api/m3u8?path=${ctx.current_rel_uri}&type=video">Videos</a>
                        </menuitem>
                    </menu>
                </details>
            ` : ``}
        </header>

        ${ctx.is_file ? `
            ${!ctx.is_readable ? `
                <main class="media"><p>Error reading file "${ctx.current_abs_path}"</p></main>
            ` : `
                <main class="media -${ctx.type}">
                    ${ctx.queue && ctx.queue.length > 1 ? `
                        <datalist id="queue">
                            ${loop(ctx.queue, (i, file) => `
                                <option value="/static${urlencode(ctx.current_rel_path + '/' + file)}"></option>
                            `)}
                        </datalist>

                        <a class="button nav -prev" href="${dirname(ctx.current_rel_uri) + '/' + urlencode(ctx.queue[ctx.queue_prev_index])}"></a>
                        <a class="button nav -next" href="${dirname(ctx.current_rel_uri) + '/' + urlencode(ctx.queue[ctx.queue_next_index])}"></a>
                    ` : ``}

                    ${ctx.type === 'image' ? `
                        <img src="/static${ctx.current_rel_uri}">
                        
                    ` : ctx.type === 'audio' ? `
                        <audio controls src="/static${ctx.current_rel_uri}"></audio>
                        <p><a class="button" href="/static${ctx.current_rel_uri}">Download</a></p>
                        
                    ` : ctx.type === 'video' ? `
                        <video controls src="/static${ctx.current_rel_uri}"></video>
                        <p><a class="button" href="/static${ctx.current_rel_uri}">Download</a></p>
                    
                    ` : ctx.type === 'text' ? `
                        <div>${ctx.content.replace(/\n/g, '<br>')}</div>

                    ` : ctx.type === 'application' ? `
                        <object data="/static${ctx.current_rel_uri}" type="application/${ctx.ext}">
                            <p>No preview available</p>
                            <p><a class="button" href="/static${ctx.urrent_rel_uri}">Download</a></p>
                        </object>

                    ` : `
                        <p>No preview available</p>
                        <p><a class="button" href="/static${ctx.current_rel_uri}">Download</a></p>
                    `}
                </main>
            `}
        ` : !ctx.is_dir ? `
            <main class="error"><p>Directory "${ctx.current_abs_path}" not found</p></main>
        
        ` : !ctx.is_readable ? `
            <main class="error"><p>Error reading directory "${ctx.current_abs_path}"</p></main>

        ` : `
            <main class="folder">
                ${loop(ctx.folders, (i, node) => `
                    <a class="item ${node.is_readable ? '' : '-locked'} ${node.is_writable ? '-writable' : ''}" href="${node.rel_uri}">
                        <div class="item-icon"><img src="/assets/icons/folder.svg"></div>
                        <p class="item-name">${node.name}</p>

                        ${node.is_readable ? `
                            <p class="item-size">${node.size} items</p>
                        ` : ''}
                        
                        <details>
                            <summary></summary>
                            <menu>
                                <p>${node.name}</p>

                                <menuitem>
                                    <form method="POST" action="/api/rename?path=${node.rel_uri}">
                                        <input type="text" name="name" placeholder="Rename ${node.name}" data-prompt="true">
                                        <button type="submit">Rename</button>
                                    </form>
                                </menuitem>
                                <menuitem>
                                    <form data-clipboard="${node.rel_uri}" data-clipboard-mode="copy">
                                        <button type="submit">Copy</button>
                                    </form>
                                </menuitem>
                                <menuitem>
                                    <form data-clipboard="${node.rel_uri}" data-clipboard-mode="move">
                                        <button type="submit">Cut</button>
                                    </form>
                                </menuitem>
                                <menuitem>
                                    <form method="POST" action="/api/trash?path=${node.rel_uri}">
                                        <button type="submit" data-confirm="Are you sure you want to move ${node.name} to the trash?">Move to trash</button>
                                    </form>
                                </menuitem>
                            </menu>
                        </details>
                    </a>
                `)}
            
                ${loop(ctx.files, (i, node) => `
                    <a class="item ${node.is_writable ? '-writable' : ''}" href="${node.rel_uri}">
                        ${node.ext === 'svg' ? `
                            <div class="item-preview -loaded">
                                <img src="/static${node.rel_uri}">
                            </div>

                        ` : node['type'] === 'image' ? `
                            <div class="item-preview">
                                <img data-src="/static${node.rel_uri}">
                            </div>

                        ` : `
                            <div class="item-icon">
                                <img src="/assets/icons/${node.icon}.svg">
                            </div>
                        
                        `}

                        <p class="item-name">${node.name}</p>
                        <p class="item-size">${node.size}</p>
                        <details>
                            <summary></summary>
                            <menu>
                                <p>${node.name}</p>

                                <menuitem>
                                    <form method="POST" action="/api/rename?path=${node.rel_uri}">
                                        <input type="text" name="name" placeholder="Rename ${node.name}" data-prompt="true">
                                        <button type="submit">Rename</button>
                                    </form>
                                </menuitem>
                                <menuitem>
                                    <form data-clipboard="${node.rel_uri}" data-clipboard-mode="copy">
                                        <button type="submit">Copy</button>
                                    </form>
                                </menuitem>
                                <menuitem>
                                    <form data-clipboard="${node.rel_uri}" data-clipboard-mode="move">
                                        <button type="submit">Cut</button>
                                    </form>
                                </menuitem>
                                <menuitem>
                                    <form method="POST" action="/api/trash?path=${node.rel_uri}">
                                        <button type="submit" data-confirm="Are you sure you want to move ${node.name} to the trash?">Move to trash</button>
                                    </form>
                                </menuitem>
                            </menu>
                        </details>
                    </a>
                `)}
                
                <div class="item -root">
                    <details>
                        <summary></summary>
                        <menu>
                            <p>${ctx.title}</p>

                            ${clipboard ? `
                                <menuitem>
                                    <form method="POST" data-clipboard="" action="/api/${clipboardMode}?from=${clipboard}&path=${ctx.current_rel_uri}">
                                        <button type="submit">Paste</button>
                                    </form>
                                </menuitem>
                            ` : ``}
                            <menuitem>
                                <details>
                                    <summary>Create playlist...</summary>
                                    <menu>
                                        <menuitem>
                                            <a href="/api/m3u8?path=${ctx.current_rel_uri}&type=audio">Audio</a>
                                        </menuitem>
                                        <menuitem>
                                            <a href="/api/m3u8?path=${ctx.current_rel_uri}&type=image">Images</a>
                                        </menuitem>
                                        <menuitem>
                                            <a href="/api/m3u8?path=${ctx.current_rel_uri}&type=video">Videos</a>
                                        </menuitem>
                                    </menu>
                                </details>
                            </menuitem>
                            <menuitem>
                                <form id="upload" method="POST" action="/api/upload?path=${ctx.current_rel_uri}">
                                    <label>
                                        <input name="files" type="file" multiple>
                                        Upload...
                                    </label>
                                </form>
                            </menuitem>
                            <menuitem>
                                <form id="new-folder" title="Create folder" method="POST" action="/api/new-folder?path=${ctx.current_rel_uri}">
                                    <input type="text" name="name" placeholder="New folder name" data-prompt="true">
                                    <button type="submit">New Folder...</button>
                                </form>
                            </menuitem>
                        </menu>
                    </details>
                </div>
            </main>
        `}
    `;

    // Load previews
    const observer = new IntersectionObserver(
        (entries, observer) => {
            for(const entry of entries) {
                if(entry.isIntersecting && !entry.target.src) {
                    entry.target.src = entry.target.dataset.src;
                }
            }
        },
        {
            root: document.querySelector('main'),
            rootMargin: '0px',
            threshold: 0.1
        }
    );

    // Context menu
    const items = document.querySelectorAll('.item');

    let touchTimer = null;
    let touchStart = 0;

    function cancelTouchTimer(e) {
        if(!touchTimer) { return; }

        clearTimeout(touchTimer);
    }

    window.ontouchmove = cancelTouchTimer;
    window.onscroll = cancelTouchTimer;

    items.forEach((item) => {
        const details = item.querySelector('details');
       
        if(!details) { return; }

        item.oncontextmenu = oncontext;

        function oncontext(e, isTouch = false) {
            e.stopPropagation();
            e.preventDefault();
            
            if(!details.open) {
                details.open = true;

                details.classList.toggle('-mobile', isTouch); 

                if(!isTouch) {
                    details.style.top = `${e.clientY}px`;
                    details.style.left = `${e.clientX}px`;

                    const menu = details.querySelector('menu');

                    if(details.offsetTop + menu.offsetHeight > window.innerHeight) {
                        details.style.top = `${e.clientY - menu.offsetHeight}px`;
                    }
                    
                    if(details.offsetLeft + menu.offsetWidth > window.innerWidth) {
                        details.style.left = `${e.clientX - menu.offsetWidth}px`;
                    }
                
                } else {
                    details.querySelector('summary').ontouchend = (e) => {
                        e.stopPropagation();
                        e.preventDefault();

                        details.open = false;
                    };
                
                }
            }
        }
    });

    // Async form submissions
    document.querySelectorAll('form').forEach(async (form) => {
        const submitButton = form.querySelector('button[type="submit"]');

        async function submit() {
            if(form.dataset.clipboardMode !== undefined) {
                clipboardMode = form.dataset.clipboardMode;
            }

            if(form.dataset.clipboard !== undefined) {
                clipboard = form.dataset.clipboard;
            }

            // Close menu
            if(!form.getAttribute('action')) {
                let parentElement = form.parentElement;

                while(parentElement) {
                    if(parentElement.tagName === 'DETAILS') {
                        parentElement.open = false;
                        break;
                    }

                    parentElement = parentElement.parentElement;
                }

            // Submit form
            } else {
                const data = {};

                for(const input of form.elements) {
                    if(!input.name) { continue; }

                    let value = input.value;

                    if(input.type === 'file') {
                        value = {};

                        for(const file of input.files) {
                            const base64 = await toBase64(file);

                            value[file.name] = base64;
                        }
                    
                    } else if(input.type === 'text' && input.dataset.prompt) {
                        value = prompt(input.placeholder);

                        if(!value) { return; }

                    }

                    data[input.name] = value;
                }
                
                document.body.classList.toggle('-loading', true);

                await fetchWithRetry(form.action, {
                    method: form.method,
                    body: JSON.stringify(data),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });

                await update();
            }
        }

        if(submitButton) {
            submitButton.onclick = (e) => {
                e.preventDefault();

                if(submitButton.dataset.confirm) {
                    if(!confirm(submitButton.dataset.confirm)) { return; }
                }

                submit();
            };

        } else {
            const submitInput = form.querySelector('input');

            if(submitInput) {
                submitInput.onchange = (e) => {
                    submit();
                };
            }
        }

        form.onsubmit = (e) => {
            e.preventDefault();
        };
    });

    for(const img of document.querySelectorAll('main .item-preview img')) {
        observer.observe(img);
        img.onload = (e) => {
            e.target.parentElement.classList.toggle('-loaded', true);
        };
    }

    // Push state
    document.querySelectorAll('main a.item, nav > a, .media > a').forEach((a) => {
        a.onclick = (e) => {
            if(e.target === a || Array.from(a.children).indexOf(e.target) > -1) {
                e.preventDefault();

                const stateName = a.getAttribute('href').split('/').pop();
                const stateURL = a.getAttribute('href');

                window.history.pushState(stateName, '', a.getAttribute('href'));

                update();
            }
        };
    });

}

// Pop state    
window.onpopstate = (e) => {
    if(!e.state) { return; }

    update();
};
    
// Left/right key controls
window.addEventListener('keyup', (e) => {
    if(e.keyCode === 27 || e.keyCode === 8) {  // Escape
        const path = location.pathname.split('/').filter(Boolean);

        path.pop();

        if(path.length > 0) {
            const stateURL = '/' + path.join('/');
            const stateName = path.pop();

            window.history.pushState(stateName, '', stateURL);

            update();
        }

    } else if(e.keyCode === 39) {               // Right
        const btnNext = document.querySelector('.button.nav.-next');

        if(btnNext) {
            const stateURL = btnNext.getAttribute('href');
            const stateName = stateURL.split('/').pop();

            window.history.pushState(stateName, '', stateURL);

            update();
        }

    } else if(e.keyCode === 37) {               // Left
        const btnPrev = document.querySelector('.button.nav.-prev');

        if(btnPrev) {
            const stateURL = btnPrev.getAttribute('href');
            const stateName = stateURL.split('/').pop();

            window.history.pushState(stateName, '', stateURL);

            update();
        }
    }
});

document.addEventListener('DOMContentLoaded', update);
