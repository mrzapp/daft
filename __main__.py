from http.server import SimpleHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qs, unquote, quote
from time import time
from argparse import ArgumentParser
from pprint import pprint

import base64
import json
import glob
import os
import mimetypes
import hashlib

# Command-line arguments
parser = ArgumentParser()
parser.add_argument('-p', '--port', type = int, default = 80, help = 'The port to expose this server on')
args = parser.parse_args()

base_dir = os.path.dirname(os.path.realpath(__file__))
trash_dir = os.path.expanduser('~') + '/.Trash-' + str(os.getuid()) + '/files'

icons = {
    'audio-x-generic': [
        '3ga',
        'aif',
        'flac',
        'm4a',
        'mp3',
        'amr',
        'ogg',
        'wav',
    ],
    'text-x-generic': [
        'md',
        'txt',
    ],
    'video-x-generic': [
        'mp4',
        'ogv',
        'mkv',
        'avi',
        'm4v',
        'wmv',
        '3gp',
        'mpg',
        'mov',
        'webm',
    ],
    'image-x-generic': [
        'jpg',
        'jpeg',
        'tga',
        'bmp',
        'gif',
        'png',
        'webp',
        'svg',
    ]
}

def get_ext(name):
    parts = os.path.splitext(name)

    if len(parts) < 2:
        return ''

    return parts[1].lower().replace('.', '')

def get_type(name, fallback = ''):
    ext = get_ext(name)

    for icon_name, extensions in icons.items():
        if ext in extensions:
            return icon_name.replace('-x-generic', '')

    return fallback

def get_readable_size(num, suffix = 'B'):
    for unit in ('', 'K', 'M', 'G', 'T', 'P', 'E', 'Z'):
        if abs(num) < 1000.0:
            return f'{num:3.1f} {unit}{suffix}'
        num /= 1000.0
    
    return f'{num:.1f} Yi{suffix}'

class Server(SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):

        super().__init__(*args, directory = base_dir, **kwargs)

    def do_POST(self):
        path = list(filter(None, urlparse(self.path).path.split('/')))
        query = parse_qs(urlparse(self.path).query)
        length = int(self.headers['Content-Length'])
        body = json.loads(self.rfile.read(length).decode('utf-8'))
        target = base_dir + '/static' + unquote(query['path'][0])

        match path[0]:
            case 'api':
                match path[1]:
                    case 'upload':
                        if 'files' in body:
                            for name, encoded_data in body['files'].items():
                                with open(target + '/' + name, 'wb') as file:
                                    file.write(base64.b64decode(encoded_data))

                    case 'trash':
                        os.system('mv "' + target + '" "' + trash_dir + '/' + os.path.basename(target) + '"')

                    case 'new-folder':
                        if not body['name']:
                            body['name'] = 'New folder'

                        os.system('mkdir "' + target + '/' + body['name'] + '"')
                    
                    case 'rename':
                        if body['name']:
                            os.system('mv "' + target + '" "' + os.path.dirname(target) + '/' + os.path.basename(body['name']) + '"')
                    
                    case 'copy':
                        if query['from']:
                            os.system('cp -r "' + base_dir + '/static' + unquote(query['from'][0]) + '" "' + target + '"')
                    
                    case 'move':
                        if query['from']:
                            os.system('mv "' + base_dir + '/static' + unquote(query['from'][0]) + '" "' + target + '"')

        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
       
        self.wfile.write(bytes('OK', 'utf-8'))

    def do_GET(self):
        # Routing
        base_uri = 'http://' + self.headers['Host'] + ':' + str(args.port)
        path = list(filter(None, urlparse(self.path).path.split('/')))
        query = parse_qs(urlparse(self.path).query)

        if len(path) < 1:
            path.append(None)

        match path[0]:
            case 'favicon.ico':
                self.send_response(404)
                self.end_headers()

            case 'api':
                response_code = 200
                content_type = 'application/json'
                data = {}

                match path[1]:
                    case 'm3u8':
                        content_type = 'application/x-mpegURL'

                        data = '#EXTM3U\n\n'

                        if 'path' in query and 'type' in query:
                            nodes = glob.glob(base_dir + '/static' + unquote(query['path'][0]) + '/**/*', recursive = True)
                            nodes = sorted(nodes, key = lambda x: x.lower())

                            for node in nodes:
                                if get_type(node) != query['type'][0]:
                                    continue

                                node_uri = node.replace(base_dir, '');

                                data += '#EXTINF:123,' + os.path.splitext(os.path.basename(node_uri))[0] + '\n'
                                data += base_uri + node_uri + '\n\n'

                    case 'upload':
                        pass

                    case 'context':
                        # Current path
                        current_rel_path = unquote(urlparse(query['path'][0]).path)
                        current_rel_path_segments = list(filter(None, current_rel_path.split('/')))
                        current_rel_path = '/' + '/'.join(current_rel_path_segments)
                        current_rel_uri = quote(current_rel_path).replace('%2F', '/')
                        current_abs_path = os.path.join(base_dir, 'static') + current_rel_path

                        data['current_rel_path'] = current_rel_path
                        data['current_rel_uri'] = current_rel_uri
                        data['current_abs_path'] = current_abs_path

                        # Set flags
                        data['is_dir'] = os.path.isdir(current_abs_path)
                        data['is_file'] = not data['is_dir'] and os.path.isfile(current_abs_path)

                        # Display file
                        if data['is_file']:
                            data['ext'] = get_ext(current_abs_path)
                            data['type'] = get_type(current_abs_path)

                            # Queue (carousel of similar files)
                            if data['type'] == 'image':
                                data['queue'] = []

                                for node in os.listdir(os.path.dirname(current_abs_path)):
                                    if get_type(node) != 'image':
                                        continue

                                    data['queue'].append(node)

                                data['queue'] = sorted(data['queue'], key = lambda x: x.lower())

                                data['queue_current_index'] = data['queue'].index(os.path.basename(data['current_rel_path']))
                                data['queue_prev_index'] = data['queue_current_index'] - 1

                                if data['queue_prev_index'] < 0:
                                    data['queue_prev_index'] = len(data['queue']) - 1

                                data['queue_next_index'] = data['queue_current_index'] + 1
                                
                                if data['queue_next_index'] >= len(data['queue']):
                                    data['queue_next_index'] = 0

                            # Text content
                            if data['type'] == 'text':
                                with open(current_abs_path, 'r') as text_file:
                                    data['content'] = text_file.read()
                                    text_file.close()

                        # List files
                        elif data['is_dir']:
                            data['files'] = []
                            data['folders'] = []

                            for node in os.listdir(current_abs_path):
                                if node.startswith('.'):
                                    continue

                                obj = {}
                                obj['name'] = os.path.basename(node)
                                obj['abs_path'] = (current_abs_path + '/' + obj['name']).replace('//', '/')
                                obj['rel_path'] = (current_rel_path + '/' + obj['name']).replace('//', '/')
                                obj['rel_uri'] = quote(obj['rel_path']).replace('%2F', '/') 

                                obj['id'] = hashlib.md5()
                                obj['id'].update(obj['rel_path'].encode('utf-8'))
                                obj['id'] = obj['id'].hexdigest()

                                obj['is_readable'] = os.access(obj['abs_path'], os.R_OK)

                                if os.path.isdir(obj['abs_path']):
                                    obj['icon'] = 'folder'

                                    if obj['is_readable']:
                                        obj['size'] = len(os.listdir(obj['abs_path']))

                                    data['folders'].append(obj)

                                else:
                                    obj['ext'] = get_ext(obj['name'])
                                    obj['size'] = get_readable_size(os.path.getsize(obj['abs_path']))
                                    obj['type'] = get_type(obj['name'], 'file')
                                    obj['icon'] = get_type(obj['name'], 'application') + '-x-generic'

                                    data['files'].append(obj)
                       
                            data['files'] = sorted(data['files'], key = lambda x: x['name'].lower())
                            data['folders'] = sorted(data['folders'], key = lambda x: x['name'].lower())

                        data['is_readable'] = os.access(current_abs_path, os.R_OK)

                        # Determine page title
                        if current_rel_path == '/':
                            data['title'] = 'Peruse'

                        else:
                            data['title'] = os.path.basename(current_rel_path)
                        
                        # Determine favicon
                        if current_rel_path == '/':
                            data['icon'] = 'user-home'
                        
                        else:
                            data['icon'] = 'folder'

                        if data['is_file']:
                            data['icon'] = 'application-x-generic'
                            
                            for icon_name, extensions in icons.items():
                                if data['ext'] in extensions:
                                    data['icon'] = icon_name
                                    break;
                        
                        # Build navigation breadcrumb
                        data['nav'] = { '/': 'Home' }
                        nav_uri = ''

                        for segment in current_rel_path_segments:
                            if not segment:
                                continue

                            nav_uri += '/' + segment

                            data['nav'][nav_uri] = segment


                self.send_response(response_code)
                self.send_header('Content-type', content_type)
                self.end_headers()
               
                if isinstance(data, dict):
                    data = json.dumps(data)

                self.wfile.write(bytes(data, 'utf-8'))

            case _:
                # Files from the "static" folder 
                if os.path.exists(base_dir + '/static' + unquote(urlparse(self.path).path)):
                    self.send_response(200)
                    self.send_header('Content-type', 'text/html')
                    self.end_headers()
                    
                    with open(os.path.join(base_dir, 'template.html'), 'r') as template_file:
                        html = template_file.read()
                        template_file.close()

                        self.wfile.write(bytes(html, 'utf-8'))
               
                # All other files
                else:
                    super().do_GET()

if __name__ == '__main__':        
    server = HTTPServer(('0.0.0.0', args.port), Server)
    print('Server started')
    
    if not os.path.isdir(trash_dir):
        os.makedirs(trash_dir)
   
    try:
        server.serve_forever()

    except KeyboardInterrupt:
        pass

    server.server_close()
    print('Server stopped')
